import json
from typing import List, Type, Tuple

from application.builders.customer_builder import CustomerBuilder


class JSONParser:
    def __init__(
        self, data: List[str], builder: Type[CustomerBuilder] = CustomerBuilder
    ):
        self.data = data
        self.builder = builder

    def parse(self) -> List[dict]:
        result = []

        for customer_obj in self.data:
            json_data, is_valid = self._convert_to_json(customer_obj)
            if not is_valid:
                continue
            else:
                customer = self.builder(json_data).build()
                result.append(customer)

        return result

    def _convert_to_json(self, data: str) -> Tuple[dict, bool]:
        try:
            return json.loads(data), True
        except ValueError:
            return {}, False
