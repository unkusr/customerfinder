from typing import List

from ._base import BaseReader
import os


class FileReader(BaseReader):
    DEFAULT_FILE = os.path.abspath("input.txt")

    def __init__(self, filename: str = DEFAULT_FILE):
        super().__init__(filename)

    def read(self) -> List[str]:
        if not self._is_file_exists():
            raise FileNotFoundError(f"File {self.filename} not found")

        with open(self.filename) as file:
            content = file.readlines()
        return content

    def _is_file_exists(self) -> bool:
        return os.path.exists(self.filename)
