import abc


class BaseReader:
    def __init__(self, filename: str):
        self.filename = filename

    @abc.abstractmethod
    def read(self):
        pass
