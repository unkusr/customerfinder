import math


class DistanceFilter:
    ALLOWED_DISTANCE = 100
    DEGREES_IN_RADIAN = 57.29577951
    MEAN_EARTH_RADIUS_KM = 6371

    DEFAULT_OFFICE_LATITUDE = 53.339428
    DEFAULT_OFFICE_LONGITUDE = -6.257664

    DEFAULT_OFFICE_LATITUDE_RADIANS = DEFAULT_OFFICE_LATITUDE / DEGREES_IN_RADIAN
    DEFAULT_OFFICE_LONGITUDE_RADIANS = DEFAULT_OFFICE_LONGITUDE / DEGREES_IN_RADIAN

    def __init__(self, customer_object: dict):
        self.customer_latitude_degrees = customer_object["latitude"]
        self.customer_longitude_degrees = customer_object["longitude"]
        self.customer_latitude_radians = (
            self.customer_latitude_degrees / self.DEGREES_IN_RADIAN
        )
        self.customer_longitude_radians = (
            self.customer_longitude_degrees / self.DEGREES_IN_RADIAN
        )

    def is_allowed_distance(self):
        return self._get_distance() <= self.ALLOWED_DISTANCE

    def _get_distance(self):
        central_angle = self._calculate_central_angle()

        return int(self.MEAN_EARTH_RADIUS_KM * central_angle)

    def _calculate_central_angle(self) -> float:
        longitude_difference = self._calculate_longitude_difference()

        central_angle = math.acos(
            math.sin(self.DEFAULT_OFFICE_LATITUDE_RADIANS)
            * math.sin(self.customer_latitude_radians)
            + math.cos(self.DEFAULT_OFFICE_LATITUDE_RADIANS)
            * math.cos(self.customer_latitude_radians)
            * math.cos(longitude_difference)
        )

        return central_angle

    def _calculate_longitude_difference(self):
        if self.DEFAULT_OFFICE_LONGITUDE_RADIANS > self.customer_longitude_radians:
            difference = (
                self.DEFAULT_OFFICE_LONGITUDE_RADIANS - self.customer_longitude_radians
            )
        else:
            difference = (
                self.customer_longitude_radians - self.DEFAULT_OFFICE_LONGITUDE_RADIANS
            )
        return difference
