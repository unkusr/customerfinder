from typing import List, Type

from application.filters.distance_filter import DistanceFilter


class CustomerResponse:
    ALLOWED_DISTANCE = 100

    def __init__(self, data: List[dict], filter: Type[DistanceFilter] = DistanceFilter):
        self.data = data
        self.filter = filter

    def result(self):
        filtered_customers = self._filter_customers()
        result = []

        for customer in filtered_customers:
            data = self._build_customer_response(customer)
            result.append(data)

        result_output = "".join(result)

        return result_output

    def _filter_customers(self) -> List[dict]:
        customers = []

        for customer in self.data:
            distance_filter = self.filter(customer)

            if distance_filter.is_allowed_distance():
                customers.append(customer)

        return sorted(customers, key=lambda obj: obj["user_id"])

    def _build_customer_response(self, customer: dict) -> str:
        return f"ID: {customer['user_id']}, NAME: {customer['name']}\n"
