class CustomerBuilder:
    def __init__(self, customer_obj: dict):
        self.customer_obj = customer_obj

    def build(self) -> dict:
        latitude = self.customer_obj.get("latitude")
        longitude = self.customer_obj.get("longitude")

        self.customer_obj["latitude"] = float(latitude)
        self.customer_obj["longitude"] = float(longitude)
        return self.customer_obj
