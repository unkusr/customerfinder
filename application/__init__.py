from typing import NoReturn

from application.parsers.json_parser import JSONParser
from application.readers.file_reader import FileReader
from application.responses.customer_response import CustomerResponse


class Application:
    @classmethod
    def run(cls) -> NoReturn:
        file_content = FileReader().read()
        json_data = JSONParser(file_content).parse()
        response = CustomerResponse(json_data)

        print(response.result())
