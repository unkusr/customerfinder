FROM python:3.8

RUN mkdir app
WORKDIR app

ENV PYTHONUNBUFFERED 1

ADD requirements /app/requirements

RUN pip install -r requirements/development.txt

ADD . /app/
