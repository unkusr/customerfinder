import pytest

from application.builders.customer_builder import CustomerBuilder


@pytest.fixture
def customer_object():
    return {
        "latitude": "53.2451025",
        "user_id": 1,
        "name": "John Doe",
        "longitude": "-6.238336",
    }


def test_builder_converts_latitude_and_longitude(customer_object):
    data = CustomerBuilder(customer_object).build()

    expected_data_type = dict
    expected_latitude_type = float
    expected_longitude_type = float

    assert isinstance(data, expected_data_type)
    assert isinstance(data["latitude"], expected_latitude_type)
    assert isinstance(data["longitude"], expected_longitude_type)
