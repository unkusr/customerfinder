import pytest

from application.parsers.json_parser import JSONParser


@pytest.fixture
def valid_data():
    return [
        '{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}\n',
    ]


@pytest.fixture
def invalid_data():
    return ['"latitude": "52.986375"\n']


def test_parse_returns_list_of_dictionaries(valid_data):
    parser = JSONParser(valid_data)

    actual_result = parser.parse()

    expected_length = 1
    expected_type_result = dict

    assert len(actual_result) == expected_length
    assert isinstance(actual_result[0], expected_type_result)


def test_parse_skip_object_if_not_valid_json(invalid_data):
    parser = JSONParser(invalid_data)

    expected_length = 0
    actual_result = parser.parse()

    assert len(actual_result) == expected_length
