import pytest

from application.filters.distance_filter import DistanceFilter


@pytest.mark.parametrize(
    "data, expected_distance",
    [
        ({"latitude": 52.986375, "longitude": -6.043701}, True),
        ({"latitude": 51.92893, "longitude": -10.27699}, False),
        ({"latitude": 54.180238, "longitude": -5.920898}, True),
    ],
)
def test_calculate_returns_correct_distance(data, expected_distance):
    d = DistanceFilter(data)

    assert d.is_allowed_distance() == expected_distance
