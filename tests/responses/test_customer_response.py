from application.responses.customer_response import CustomerResponse
import pytest

CORRECT_CUSTOMER = {
    "latitude": 53.1229599,
    "user_id": 6,
    "name": "Theresa Enright",
    "longitude": -6.2705202,
}

INVALID_CUSTOMER = {
    "latitude": 52.2559432,
    "user_id": 9,
    "name": "Jack Dempsey",
    "longitude": -7.1048927,
}


@pytest.fixture
def data():
    return [CORRECT_CUSTOMER, INVALID_CUSTOMER]


def test_result_returns_correct_records(data):
    actual_result = CustomerResponse(data).result()

    assert "ID: 6, NAME: Theresa Enright" in actual_result
    assert "ID: 9, NAME: Jack Dempsey" not in actual_result
