import os

import pytest

from application.readers.file_reader import FileReader


def get_file_path(filename):
    return f"{os.getcwd()}/tests/fixtures/{filename}"


def test_read_raises_error_when_file_does_not_exists():
    with pytest.raises(FileNotFoundError):
        FileReader("bad-filename").read()


def test_read_returns_file_content():
    filename = get_file_path("test.txt")
    reader = FileReader(filename)

    expected_data = "data"
    actual_data = reader.read()

    assert actual_data[0] == expected_data
